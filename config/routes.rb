Rails.application.routes.draw do
  
  
  

  namespace :admin do
    resources :users
  	resources :papers    
    resources :questions 
  end

  resources :papers do 
  	member do 
  		get :participate
  		patch :answer
      get :result 
  	end
  end

  

  devise_for :users
  root "home#index"

  namespace :api do 
      resources :sessions, only: [:create, :destroy]
      resources :registrations, only: [:create]
      
      resources :papers do 
        member do 
          get :participate
          patch :answer
          get :result 
        end
      end
      resources :users do 
        collection{get :top}
      end

      resources :admin_papers

  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

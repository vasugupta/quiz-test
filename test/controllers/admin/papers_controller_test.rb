require 'test_helper'

class Admin::PapersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_papers_index_url
    assert_response :success
  end

  test "should get new" do
    get admin_papers_new_url
    assert_response :success
  end

  test "should get show" do
    get admin_papers_show_url
    assert_response :success
  end

  test "should get edit" do
    get admin_papers_edit_url
    assert_response :success
  end

end

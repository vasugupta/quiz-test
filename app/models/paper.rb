class Paper < ApplicationRecord
	has_and_belongs_to_many :questions	
	has_many :paper_attempts
	before_create :set_number_


	def to_param
		set_number
	end

	def set_number_
		self.set_number = "LPU" + SecureRandom.random_number(10**9).to_s(10).rjust(9, '0').upcase
	end

	def top_10_user
		paper_attempts.order("final_score desc").limit(10)
	end


	
end

class Question < ApplicationRecord
	has_many :options, dependent: :destroy
	enum question_type: %i{scq mcq}

	validates_presence_of :body, :question_type

	accepts_nested_attributes_for :options, allow_destroy: true
end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :paper_attempts
  def admin?
  	self.role == "admin"
	end       

	def create_and_get_api_key
		self.token = Digest::SHA1.hexdigest "#{Time.now.to_i}#{1}"
		self.save
		return self.token
	end
end

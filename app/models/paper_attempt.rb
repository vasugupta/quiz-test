class PaperAttempt < ApplicationRecord
	belongs_to :paper
	belongs_to :user
	has_many :paper_attempt_answers, dependent: :destroy
	has_many :options, through: :paper_attempt_answers 


	accepts_nested_attributes_for :paper_attempt_answers

	def self.over_all_10
		self.order("final_score desc").limit(10)
	end

	def check_score
		correct_ans = self.options.select{|a| a.is_correct == true}.count
		wrong_ans = self.options.select{|a| a.is_correct == false}.count
		f_s = 0
		f_s = (correct_ans * 10) + (wrong_ans * (-5))
		f_s
		self.final_score = f_s
		self.save
	end
end

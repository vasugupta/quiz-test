class Admin::QuestionsController < ApplicationController
  before_action :check_admin?
  before_action :set_question, only: [:edit, :update, :destroy, :show]	

  def index
  	@questions = Question.all
  end

  def new
  	@question = Question.new
  end

  

  def destroy
  	if @question.destroy
  		flash[:success] = "Question has been removed"	
  		redirect_to admin_questions_path
  	else
  		flash[:error] = "#{@question.errors.full_messages.join(',')}"	
  		redirect_to admin_questions_path
  	end
  end

  def create
  	@question = Question.new(question_param)
  	if @question.save
  		flash[:success] = "Question has been created"	
  		redirect_to admin_questions_path
  	else
  		flash[:error] = "#{@question.errors.full_messages.join(',')}"	
  		render(:new)
  	end
  end

  def show
  end

  def edit
  end

  def update
  	if @question.update(question_param)
  		flash[:success] = "Question has been updated"	
  		redirect_to admin_questions_path
  	else
  		flash[:error] = "#{@question.errors.full_messages.join(',')}"	
  		render(:edit)
  	end
  end

  private
  	def set_question
  		@question = Question.find_by(id: params[:id])	
  	end

	  def question_param
	  	params.require(:question).permit(:body, :question_type, options_attributes: [:id, :body, :is_correct])
	  end
end

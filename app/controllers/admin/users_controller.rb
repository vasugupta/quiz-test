class Admin::UsersController < ApplicationController
  before_action :check_admin?


  def index
  	@users = User.all - User.where(role: "admin")
  end
end

class PapersController < ApplicationController
	before_action :authenticate_user!

	before_action :set_paper 


	def participate
		if current_user.paper_attempts.collect(&:paper_id).include?(@paper.id)
			flash[:warning] = "You have already attempted this quiz."
			redirect_to root_url
		else
			@attempt = current_user.paper_attempts.new(paper_id: @paper.id)
			if @attempt.save
				flash[:warning] = "You have stated this quiz do not reload or leave it without complete."
			else
				flash[:error] = "#{attempt.errors.full_messages.join(',')}"
				redirect_to root_url
			end
		end
	end


	def answer
		@paper_attempt = current_user.paper_attempts.find_by(id: params[:paper_attempt_id])
		if @paper_attempt.update(paper_attempt_params)
			@paper_attempt.check_score
			flash[:success] = "Your quiz is submitted"
			redirect_to root_url
		else
			flash[:error] = "#{@paper_attempt.errors.full_messages.join(',')}"
			redirect_to root_url
		end
	end

	def result
		@paper_attempt = current_user.paper_attempts.find_by(paper_id: @paper.id)
		if @paper_attempt.blank?
			flash[:error] = "You have not attempted this quiz."
			redirect_to root_url
		end
	end

	private
		def paper_attempt_params
			params.require(:paper_attempt).permit(paper_attempt_answers_attributes: [:option_id])	
		end

		def set_paper
				@paper = Paper.find_by(set_number: params[:id])
		end
end

module Api
		class UsersController < Api::ApplicationController
			before_action :check_admin_user!

			def index
				 @users = User.all - User.where(role: "admin")

				 render json: {
				 	success: true,
				 	users: @users.as_json
				 }
			end

			def top
				@all_attempts = PaperAttempt.over_all_10
				 render json: {
				 	success: true,
				 	attempts: @all_attempts.as_json(
				 		include: :user
				 		)
				 }
			end
			
		end
end
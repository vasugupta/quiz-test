module Api
		class SessionsController < Api::ApplicationController
					skip_before_action :check_user!

					def create
						@user = User.find_by_email(params[:user][:email])
						if @user.present? 
							if @user.valid_password?(params[:user][:password]) 
								@apikey = @user.create_and_get_api_key 
									render json: {
										success: true,
										message: "successfully signin"+ (@user.admin? ? " with admin" : ""),
										access_token: @apikey
									}
								
							else
								render json: {
									success: false,
									message: "Entered wrong password"
								}
							end
						else
							render json: {
								success: false,
								message: "User doesn't exist"
							}
						end
					end


					def destroy
						@apikey = ApiKey.where(access_token: params[:id]).last
						if @apikey && @apikey.expired?
							render json: {
								success: false,
								message: "Already logout!"
							}
						else
							@apikey.make_expire
							render json: {
								success: true,
								message: "successfully logout!"
							}
						end
					end



					
		end
end
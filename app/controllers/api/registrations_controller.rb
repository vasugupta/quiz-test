module Api
		class RegistrationsController < Api::ApplicationController
			skip_before_action :check_user!

			def create
				@user = User.new(user_params)
				if @user.save
					key = @user.create_and_get_api_key
					render json: {
		       success: true,
		       message: "successfully signup",
		       access_token:  key
		      }
				else
					render json: {
		       success: false,
		       message: @user.errors.full_messages.join(", ")
		      }
				end

			end

			private
				def user_params
					params.require(:user).permit(:email, :password)
				end
		end
end
module Api
		class PapersController < Api::ApplicationController
			before_action :set_paper, only: [:participate]
			def index
				@papers = Paper.all
				render json: {
					success: true,
					papers: @papers.as_json
				}
			end


			def participate
				if current_user.paper_attempts.collect(&:paper_id).include?(@paper.id)
					render json: {
						success: false,
						message: "You have already attempted this quiz."	
					}
				else
					@attempt = current_user.paper_attempts.new(paper_id: @paper.id)
					if @attempt.save
						render json: {
							success: true,
							questions: @paper.as_json(include: { questions: {
                           include:  [:options ]
                         } })
						}
					else
						render json: {
							success: false,
							message: "#{attempt.errors.full_messages.join(',')}"
						}
					end
				end
			end


			private
					def paper_attempt_params
						params.require(:paper_attempt).permit(paper_attempt_answers_attributes: [:option_id])	
					end

					def set_paper
							@paper = Paper.find_by(set_number: params[:id])
					end					

					
		end
end
module Api
		class ApplicationController < ActionController::API
			before_action :check_user!


			private
			
				def check_user!
					@user = User.find_by(token: params[:access_token])
					unless @user
						render json: {
							success: false,
							message: "access denied!"
						}
					else
						@current_user = @user
					end
				end

				def check_admin_user!
					@user = User.find_by(token: params[:access_token])
					if @user && @user.admin?
						@current_user = @user
					else
						render json: {
							success: false,
							message: "access denied!"
						}
					end
				end

				def successfully_sent?(resource)
			    notice = if Devise.paranoid
			      resource.errors.clear
			      :send_paranoid_instructions
			    elsif resource.errors.empty?
			      :send_instructions
			    end

			    if notice
			      # set_flash_message! :notice, notice
			      true
			    end
			  end

		    def set_minimum_password_length
			    # if devise_mapping.validatable?
			      @minimum_password_length = 6
			    # end
			  end

		end
end
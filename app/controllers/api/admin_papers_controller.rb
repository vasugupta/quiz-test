module Api
		class AdminPapersController < Api::ApplicationController

		  before_action :check_admin_user!
		  before_action :set_paper, only: [:update, :edit, :show]
		  
		  def index
		  	@papers = Paper.all
		  end

		  def new
		  end

		  def create
		  	@paper = Paper.new(paper_params)
		  	if @paper.save
		  		render json: {
		  			success: true,
		  			paper: @paper.as_json
		  		}
		  	else
		  		render json: {
			  		success: false,
			  		message: "#{@paper.errors.full_messages.join(',')}"	
		  		}
		  	end
		  end

		  def show
		  end

		  def edit
		    @questions = Question.all
		  end

		  def update
		    if @paper.update(paper_params)
		      flash[:success] = "paper has been updated"
		      redirect_to edit_admin_paper_path(@paper)
		    else
		      flash[:error] = "#{@paper.errors.full_messages.join(',')}"
		      render(:edit)
		    end
		  end

		  private 
		    def set_paper
		     @paper = Paper.find_by(set_number: params[:id]) 
		    end

		  	def paper_params
		  		params.require(:paper).permit(:name, question_ids: [])
		  	end
		end
end
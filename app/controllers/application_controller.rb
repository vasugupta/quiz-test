class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  def check_admin?
  	if current_user && current_user.admin?
  	else
  		flash[:error] = "You are not authorized"
  		redirect_to root_url
  	end
  end

end

class CreatePapersQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :papers_questions do |t|
      t.integer :paper_id
      t.integer :question_id

      t.timestamps
    end
  end
end

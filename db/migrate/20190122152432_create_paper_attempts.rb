class CreatePaperAttempts < ActiveRecord::Migration[5.0]
  def change
    create_table :paper_attempts do |t|
      t.integer :user_id
      t.integer :paper_id
      t.datetime :completed_at
      t.integer :final_score

      t.timestamps
    end
  end
end

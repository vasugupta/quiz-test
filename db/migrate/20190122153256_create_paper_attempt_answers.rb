class CreatePaperAttemptAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :paper_attempt_answers do |t|
      t.integer :paper_attempt_id
      t.integer :option_id

      t.timestamps
    end
  end
end

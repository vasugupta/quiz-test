class AddCorrectAnsQuestion < ActiveRecord::Migration[5.0]
  def change
  	add_column :questions, :correct_answer, :text
  	rename_column :questions, :type, :question_type 
  end
end

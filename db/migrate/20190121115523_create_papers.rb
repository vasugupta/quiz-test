class CreatePapers < ActiveRecord::Migration[5.0]
  def change
    create_table :papers do |t|
      t.string :set_number
      t.boolean :active

      t.timestamps
    end
  end
end
